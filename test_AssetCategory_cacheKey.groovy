import com.liferay.asset.kernel.model.AssetCategory
import com.liferay.asset.kernel.model.AssetVocabulary
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil
import com.liferay.portal.kernel.dao.orm.QueryUtil
import com.liferay.portal.kernel.model.Group
import com.liferay.portal.kernel.model.GroupConstants
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal
import com.liferay.portal.kernel.service.GroupLocalServiceUtil
import com.liferay.portal.kernel.service.ServiceContext
import com.liferay.portal.kernel.util.FriendlyURLNormalizerUtil
import com.liferay.portal.kernel.util.LocaleUtil
import com.liferay.portal.kernel.util.OrderByComparator
import com.liferay.portal.kernel.util.PortalUtil
import com.liferay.portal.kernel.util.StackTraceUtil
import com.liferay.portal.kernel.util.StringPool

String groupName = "Test Group for LPSP-234"
Group group = null;
try {
    long companyId = PortalUtil.getDefaultCompanyId();
    long userId = PrincipalThreadLocal.getUserId();
    group = addGroup(companyId, userId, GroupConstants.DEFAULT_PARENT_GROUP_ID, groupName)
    println "Created group: " + groupName + " with groupId: " + group.getGroupId()

   testGetSortedVocabularyCategories(group);

}
catch (Exception e) {
    println "Script failed: " + e.toString();
    println StackTraceUtil.getStackTrace(e);
}
finally {
    if (group != null) {
        println "Cleaning up by deleting group..."
        GroupLocalServiceUtil.deleteGroup(group)
    }

}

//Based on GroupTestUtil.addGroup
def static Group addGroup(long companyId, long userId, long parentGroupId, String name) throws Exception {

    Group group = GroupLocalServiceUtil.fetchGroup(companyId, name);

    if (group != null) {
        return group;
    }

    Map<Locale, String> nameMap = new HashMap<>();

    nameMap.put(LocaleUtil.getDefault(), name);

    Map<Locale, String> descriptionMap = new HashMap<>();

    //CUSTOM START
    descriptionMap.put(LocaleUtil.getDefault(), "description");
    //CUSTOM END

    int type = GroupConstants.TYPE_SITE_OPEN;
    String friendlyURL =
            StringPool.SLASH + FriendlyURLNormalizerUtil.normalize(name);
    boolean site = true;
    boolean active = true;
    boolean manualMembership = true;
    int membershipRestriction =
            GroupConstants.DEFAULT_MEMBERSHIP_RESTRICTION;

    //CUSTOM START
    return GroupLocalServiceUtil.addGroup(
            userId, parentGroupId, null, 0,
            GroupConstants.DEFAULT_LIVE_GROUP_ID, nameMap, descriptionMap, type,
            manualMembership, membershipRestriction, friendlyURL, site, active,
            new ServiceContext());
    //CUSTOM END
}


def static void testGetSortedVocabularyCategories(Group group) throws Exception {
    // code copied from testDeleteCategory() method to quickly get going.
    Map<Locale, String> titleMap = new HashMap<>();

    titleMap.put(LocaleUtil.US, "title" + System.currentTimeMillis());

    long groupId = group.getGroupId();
    long userId = group.getCreatorUserId();
    ServiceContext serviceContext = new ServiceContext();
    serviceContext.setScopeGroupId(group.getGroupId());
    serviceContext.setUserId(userId);

    AssetVocabulary assetVocabulary =
            AssetVocabularyLocalServiceUtil.addVocabulary(
                    userId, groupId,
                    "assetVocab" + System.currentTimeMillis(), titleMap, null, null,
                    serviceContext);

    AssetCategory assetCategory = AssetCategoryLocalServiceUtil.addCategory(
            userId, groupId,
            "assetCategory1", assetVocabulary.getVocabularyId(),
            serviceContext);

    assetCategory = AssetCategoryLocalServiceUtil.addCategory(
            userId, groupId,
            "assetCategory2", assetVocabulary.getVocabularyId(),
            serviceContext);

    assetCategory = AssetCategoryLocalServiceUtil.addCategory(
            userId, groupId,
            "assetCategory3", assetVocabulary.getVocabularyId(),
            serviceContext);

    // uncomment the following test to see that the anonymous inner class returns null
    // from the toString() method.
    String value = new OrderByComparator<AssetCategory>() {
        @Override
        public int compare(AssetCategory o1, AssetCategory o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }
    }.toString();
    // Assert.assertNotNull("comparator string is null", value);

    // This will cause an NPE because the anonymous inner class returns null
    // from the toString() method of the instance.
    List<AssetCategory> sortedCategories = AssetCategoryLocalServiceUtil.
            getVocabularyCategories(
                    assetVocabulary.getVocabularyId(),
                    QueryUtil.ALL_POS, QueryUtil.ALL_POS,
                    new OrderByComparator<AssetCategory>() {
                        @Override
                        public int compare(AssetCategory o1, AssetCategory o2) {
                            return o1.getTitle().compareTo(o2.getTitle());
                        }
                        @Override
                        public String getOrderBy() {
                            return "";
                        }
                    });

    println "sorted categories list should not be null, isNull=" + (sortedCategories == null);
    println "List of asset categories should be 3, size=" + sortedCategories.size();
}