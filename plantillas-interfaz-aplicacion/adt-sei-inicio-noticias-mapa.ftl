<#-- Liferay versión 7.2 -->
<script>
    $(document).ready(function() {
        /* Funcionalidad carousel de slick.js */
        $('.carousel-slick').slick({
            slidesToShow: 2,
            dots:true,
            autoplay: true,
            autoplaySpeed: 10000,
            arrows: false,
            responsive: [
                {
                  breakpoint: 991,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
        });
    });
</script>

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<!-- configurarlo desde el panel de control -> configuracion ->  configuracion del sistema -> motores de plantilla (eliminar servicelocator de variables restringidas) -->
<#--<#assign AssetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>-->
<#--${AssetEntryLocalService}-->
<#--<#assign LayoutLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.LayoutLocalService")>-->
<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext()>
<#assign themeDisplay = serviceContext.getThemeDisplay() />
<#-- themeDisplay.getLayout() sacar el objeto con todos los parametros -->
<#assign Layout = themeDisplay.getLayout() />
<#assign pagina = Layout.friendlyURL />
<#--${Layout}-->
<#--${pagina}-->
<div class="container seiasa-inicio">
    <div class="row mb-2">
        <div class="col-md-6 col-lg-8">
            <h1>Actualidad Seiasa</h1>
            <div class="contenido-cabcolor fd-gris-claro rounded">
                <div class="carousel-slick">
                    <#list entries as curEntry>
                        <#assign assetRenderer = curEntry.getAssetRenderer() />
                        <#assign className = assetRenderer.getClassName() />
                        <#--<#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, CurEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />-->

                        <#if className =="com.liferay.journal.model.JournalArticle">
                            <#assign JournalArticle = assetRenderer.getArticle() />
                            <#assign document = saxReaderUtil.read(JournalArticle.getContent()) />
                            <#assign Titulo = document.valueOf("//dynamic-element[@name='tituloPrincipal']/dynamic-content/text()") />
                            <#assign img = document.valueOf("//dynamic-element[@name='imagenPrincipal']/dynamic-content/text()") />
                            <#assign imgGroupId = img?eval.groupId/>
                            <#assign imgName = img?eval.name/>
                            <#assign imgUuid = img?eval.uuid/>
                            <#assign imgAlt = img?eval.alt/>
                            <#assign Fecha = document.valueOf("//dynamic-element[@name='fechaNoticia']/dynamic-content/text()") />
                            <#assign Descripcion = document.valueOf("//dynamic-element[@name='descripcionNoticia']/dynamic-content/text()") />
                            <#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, currentURL) />
                            <#assign viewURL2 = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, curEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />
                            <#--<#assign pageLayout = LayoutLocalService.getLayout(groupId, false, layoutID) />-->
                        </#if>

                        <#--<div class="asset-abstract">
                            <div class="pull-right">-->
                                <#--<@getPrintIcon />-->

                                <#--<@getFlagsIcon />-->

                                <#--<@getEditIcon />-->
                            <#--</div>-->

                            <#--<h3 class="asset-title">
                                <a href="${viewURL}">
                                    ${entryTitle}
                                </a>
                            </h3>-->
                            <#--<@getEditIcon />-->
                            <div class="carusel-contenedor-noticia">
                                <div class="lfr-meta-actions asset-actions">
                                    <@getEditIcon />
                                </div>
                                <div class="sei-img-inicio">
									<#if imgGroupId?? && imgGroupId != "">
                                    	<img class="img-responsive" src="/documents/${imgGroupId}/0/${imgName}/${imgUuid}" alt="${imgAlt}">
									<#else>
										<img class="img-responsive no-imagen" src="/documents/20124/0/no-img.png/26d39fab-a07b-b4c4-c30b-88e859f6cbe7" alt="No hay imagen">
									</#if>
                                </div>
                                <div class="carusel-fecha txt-verde">${Fecha?date("yyyy-MM-dd")?string("dd/MM/yyyy")}</div>
                                <div class="carusel-titulo" recortar-literal="100" data-toggle="tooltip" data-placement="bottom" title="${Titulo}">${Titulo}</div>
                                <div class="my-2"><a class="btn btn-outline-primary px-5" href="${viewURL}" alt="ir al detalle de la noticia">Ver más</a></div>
                            </div>
                                
                            <#--<@getMetadataField fieldName="tags" />-->

                            <#--<@getMetadataField fieldName="create-date" />-->

                            <#--<@getMetadataField fieldName="view-count" />-->

                            <#--<div class="asset-content">-->
                                <#--<@getSocialBookmarks />-->

                                <#--<div class="asset-summary">-->
                                    <#--<@getMetadataField fieldName="author" />-->

                                    <!--${htmlUtil.escape(assetRenderer.getSummary(renderRequest, renderResponse))}-->

                                    <#--<a href="${viewURL}"><@liferay.language key="read-more" /><span class="hide-accessible"><@liferay.language key="about" />${entryTitle}--></span></a>
                                <#--</div>-->

                                <#--<@getRatings />-->

                                <#--<@getRelatedAssets />-->

                                <#--<@getDiscussion />-->
                            <#--</div>-->
                        <#--</div>-->
                    </#list>
                </div>
            </div>
            <div class="col mb-4 mb-md-0 text-center">
                <a class="enlace-actualidad" href="/noticias">Más Noticias</a>
            </div><#-- Final del col-md-8 -->
        </div><#-- Final del col-md-8 -->
        <div class="col-md-6 col-lg-4">
            <h1>Mapa de Actuaciones</h1>
            <div class="contenido-cabcolor fd-gris-claro text-center rounded">
                <img class="img-responsive" src="/documents/20124/0/Imagen-5.png" alt="imagen mapa">
                <a href="/mapa-de-actuaciones" class="btn btn-outline-primary my-3 px-5">Ver mapa</a>
            </div>
        </div><#-- Final del col-md-8 -->
    </div>
</div>
<#-- Final de la estructura de la plantilla  -->

<#-- Macros -->

<#macro getDiscussion>
	<#if getterUtil.getBoolean(enableComments) && assetRenderer.isCommentable()>
		<br />

		<#assign discussionURL = renderResponse.createActionURL() />

		${discussionURL.setParameter("javax.portlet.action", "invokeTaglibDiscussion")}

		<@liferay_comment["discussion"]
			className=curEntry.getClassName()
			classPK=curEntry.getClassPK()
			formAction=discussionURL?string
			formName="fm" + curEntry.getClassPK()
			ratingsEnabled=getterUtil.getBoolean(enableCommentRatings)
			redirect=currentURL
			userId=assetRenderer.getUserId()
		/>
	</#if>
</#macro>

<#macro getEditIcon>
	<#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
		<#assign redirectURL = renderResponse.createRenderURL() />

		${redirectURL.setParameter("struts_action", "/asset_publisher/add_asset_redirect")}
		${redirectURL.setWindowState("pop_up")}

		<#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />

		<#if validator.isNotNull(editPortletURL)>
			<#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />

			<@liferay_ui["icon"]
				cssClass="icon-monospaced visible-interaction"
				icon="pencil"
				markupView="lexicon"
				message="Editar: " +Titulo
				url="javascript:Liferay.Util.openWindow({dialog: {width: 960}, id:'" + renderResponse.getNamespace() + "editAsset', title: 'Editar: " + Titulo + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
			/>
		</#if>
	</#if>
</#macro>

<#macro getFlagsIcon>
	<#if getterUtil.getBoolean(enableFlags)>
		<@liferay_flags["flags"]
			className=curEntry.getClassName()
			classPK=curEntry.getClassPK()
			contentTitle=curEntry.getTitle(locale)
			label=false
			reportedUserId=curEntry.getUserId()
		/>
	</#if>
</#macro>

<#macro getMetadataField
	fieldName
>
	<#if stringUtil.split(metadataFields)?seq_contains(fieldName)>
		<span class="metadata-entry metadata-${fieldName}">
			<#assign dateFormat = "dd MMM yyyy - HH:mm:ss" />

			<#if stringUtil.equals(fieldName, "author")>
				<@liferay.language key="by" /> ${htmlUtil.escape(portalUtil.getUserName(assetRenderer.getUserId(), assetRenderer.getUserName()))}
			<#elseif stringUtil.equals(fieldName, "categories")>
				<@liferay_asset["asset-categories-summary"]
					className=curEntry.getClassName()
					classPK=curEntry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif stringUtil.equals(fieldName, "create-date")>
				${dateUtil.getDate(curEntry.getCreateDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "expiration-date")>
				${dateUtil.getDate(curEntry.getExpirationDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "modified-date")>
				${dateUtil.getDate(curEntry.getModifiedDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "priority")>
				${curEntry.getPriority()}
			<#elseif stringUtil.equals(fieldName, "publish-date")>
				${dateUtil.getDate(curEntry.getPublishDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "tags")>
				<@liferay_asset["asset-tags-summary"]
					className=curEntry.getClassName()
					classPK=curEntry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif stringUtil.equals(fieldName, "view-count")>
				${curEntry.getViewCount()} <@liferay.language key="views" />
			</#if>
		</span>
	</#if>
</#macro>

<#macro getPrintIcon>
	<#if getterUtil.getBoolean(enablePrint)>
		<#assign printURL = renderResponse.createRenderURL() />

		${printURL.setParameter("mvcPath", "/view_content.jsp")}
		${printURL.setParameter("assetEntryId", curEntry.getEntryId()?string)}
		${printURL.setParameter("viewMode", "print")}
		${printURL.setParameter("type", curEntry.getAssetRendererFactory().getType())}
		${printURL.setWindowState("pop_up")}

		<@liferay_ui["icon"]
			icon="print"
			markupView="lexicon"
			message="print"
			url="javascript:Liferay.Util.openWindow({id:'" + renderResponse.getNamespace() + "printAsset', title: '" + languageUtil.format(locale, "print-x-x", ["hide-accessible", entryTitle], false) + "', uri: '" + htmlUtil.escapeURL(printURL.toString()) + "'});"
		/>
	</#if>
</#macro>

<#macro getRatings>
	<#if getterUtil.getBoolean(enableRatings) && assetRenderer.isRatable()>
		<div class="asset-ratings">
			<@liferay_ui["ratings"]
				className=curEntry.getClassName()
				classPK=curEntry.getClassPK()
			/>
		</div>
	</#if>
</#macro>

<#macro getRelatedAssets>
	<#if getterUtil.getBoolean(enableRelatedAssets)>
		<@liferay_asset["asset-links"]
			assetEntryId=curEntry.getEntryId()
			viewInContext=!stringUtil.equals(assetLinkBehavior, "showFullContent")
		/>
	</#if>
</#macro>

<#macro getSocialBookmarks>
	<@liferay_social_bookmarks["bookmarks"]
		className=curEntry.getClassName()
		classPK=curEntry.getClassPK()
		displayStyle="${socialBookmarksDisplayStyle}"
		target="_blank"
		title=curEntry.getTitle(locale)
		url=viewURL
	/>
</#macro>