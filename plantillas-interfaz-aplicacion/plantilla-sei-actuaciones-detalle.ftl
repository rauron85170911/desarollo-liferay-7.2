<#-- Plantilla Actuaciones Detalle -->

<script>
    $(document).ready(function(){
        //var $titulo = $('#content > h1').text();
        //console.log($titulo);
        $titulo = $('#content > h1').remove();
        setTimeout(function(){ $(window).scrollTop(0); },300);
        $contenidoAsset = $('.asset-full-content');
        $contenidoAsset.find('h4.component-title').remove();
        $contenidoAsset.find('.metadata-author').remove();
        $contenidoAsset.find('.lfr-asset-anchor').remove();

        if(screen.width < 768) {
            $('.carusel').slick({
                slidesToShow: 1,
                dots:false,
                autoplay: true,
                autoplaySpeed: 5000,
                prevArrow: '<div class="anterior"><i class="icon-angle-left"></i></div>',
                nextArrow: '<div class="siguiente"><i class="icon-angle-right"></i></div>',
            });

            $('.carusel').slickLightbox({
                slick: {
                    itemSelector: 'a',
                    navigateByKeyboard: true,
                    slidesToShow: 1,
                    dots:false,
                    autoplay: true,
                    autoplaySpeed: 5000
                }
            });

        } else {
            $('.carusel').slick({
                slidesToShow: 3,
                dots:false,
                autoplay: true,
                autoplaySpeed: 5000,
                prevArrow: '<div class="anterior"><i class="icon-angle-left"></i></div>',
                nextArrow: '<div class="siguiente"><i class="icon-angle-right"></i></div>',
            });

            $('.carusel').slickLightbox({
                slick: {
                    itemSelector: 'a',
                    navigateByKeyboard: true,
                    slidesToShow: 3,
                    dots:false,
                    autoplay: true,
                    autoplaySpeed: 5000
                }
            });

        }
    });
</script>

<#assign isSignedIn = themeDisplay.isSignedIn() >
<#assign url = themeDisplay.getCDNBaseURL()>
<#assign groupId = themeDisplay.getScopeGroupId()>
<#assign Layout = themeDisplay.getLayout()>
<#assign journalArticleId = .vars['reserved-article-id'].data>

<#--${url}<br>${groupId}<br>${Layout}<br>${journalArticleId}-->

<div class="contenedor-actuacion-detalle">
    <h1 class="text-center mb-4">${nombreActuacion.getData()}
        <#if isSignedIn>
            <span class="ica-edicion-detalle">
                <a href="${url}/group/guest/~/control_panel/manage?p_p_id=com_liferay_journal_web_portlet_JournalPortlet&_com_liferay_journal_web_portlet_JournalPortlet_mvcPath=%2Fedit_article.jsp&_com_liferay_journal_web_portlet_JournalPortlet_groupId=${groupId}&_com_liferay_journal_web_portlet_JournalPortlet_articleId=${journalArticleId}" target="_blank">
                    <@liferay_ui["icon"]
                                icon="pencil"
                                markupView="lexicon"
                                message="Editar: " + nombreActuacion.getData()
                            />
                </a>
            </span>
        </#if>
    </h1>
    <div class="row">
        <div class="col-md-12 text-center align-self-center">
            <#if caruselImagenes.getSiblings()?has_content>
                <div class="carusel">
                    <#list caruselImagenes.getSiblings() as itemImagenes>
                        <#if itemImagenes.getData()?? && itemImagenes.getData() != "">
                            <div><a href="${itemImagenes.getData()}"><img width="260" height="180" alt="${itemImagenes.getAttribute('alt')}" src="${itemImagenes.getData()}" /></a></div>
                        <#else>
                            <div><img width="117" height="117" alt="No hay imagen" src="/documents/20124/0/no-img.png/26d39fab-a07b-b4c4-c30b-88e859f6cbe7" /></div>
                        </#if>
                    </#list>
                </div>
            </#if>
        </div>
        <div class="col-md-7 mt-4 contenedor-datos">
            <h2 class="txt-gris mt-4">Objetivo</h2>
            <#if ObjetivoActuacion.getData()?? && ObjetivoActuacion.getData() != "">
                <p class="mt-3">${ObjetivoActuacion.getData()}</p>
            <#else>
                <p class="mt-3">No hay Objetivo</p>
            </#if>
            <h2 class="txt-gris">Descripción</h2>
            <#if DescripcionActuacion.getData()?? && DescripcionActuacion.getData() != "">
                <p class="mt-3">${DescripcionActuacion.getData()}</p>
            <#else>
                <p class="mt-3">No hay Descripción</p>
            </#if>
            <h2 class="txt-gris">Ubicación</h2>
            <#if ubicacionActuacion.getData()?? && ubicacionActuacion.getData() != "">
                <p class="mt-3">${ubicacionActuacion.getData()}</p>
            <#else>
                <p class="mt-3">No hay Ubicación</p>
            </#if>
            <h2 class="txt-gris">Estado</h2>
            <#if estadoActuacion.getData()?? && estadoActuacion.getData() != "">
                <p class="mt-3">${estadoActuacion.getData()}</p>
            <#else>
                <p class="mt-3">No hay Estado</p>
            </#if>
        </div>
        <div class="col-md-5 contenedor-cabcolor mt-4">
            <div class="cabecera-cabcolor fd-gris-medio-oscuro contenedor-actuaciones-noticias">
                <h2 class="sin-borde">Noticias de esta Actuacion</h2>
            </div>
            <div class="borde-gris">
                <#if separadorNoticiaActuacion.getSiblings()?has_content>
                    <#list separadorNoticiaActuacion.getSiblings() as item>
                        <#if item.fechaNoticiaActuacion.getData()?? && item.fechaNoticiaActuacion.getData() != "" && item.descripcionNoticiaActuacion.getData()?? && item.descripcionNoticiaActuacion.getData() != "">
                            <#assign separadorNoticiaActuacion_fechaNoticiaActuacion_Data = getterUtil.getString(item.fechaNoticiaActuacion.getData())>
                            <#if validator.isNotNull(separadorNoticiaActuacion_fechaNoticiaActuacion_Data)>
                                <#assign separadorNoticiaActuacion_fechaNoticiaActuacion_DateObj = dateUtil.parseDate("yyyy-MM-dd", separadorNoticiaActuacion_fechaNoticiaActuacion_Data, locale)>
                                <div class="fecha-actuacion-noticia mt-2">${dateUtil.getDate(separadorNoticiaActuacion_fechaNoticiaActuacion_DateObj, "dd/MM/yyyy", locale)}</div>
                            </#if>
                                <div class="contenedor-actuacion-noticia">
                                    <p>${item.descripcionNoticiaActuacion.getData()}</p>
                                </div>
                        <#else>
                            <p class="text-center px-5 py-5">no existen noticias de esta actuación</p>
                        </#if>
                    </#list>
                </#if>
            </div>                            
        </div>
    </div>
</div>