<#-- Plantilla Noticias Detalle -->
<script>
    $(document).ready(function(){
        //var $titulo = $('#content > h1').text();
        //console.log($titulo);
        $titulo = $('#content > h1').remove();
        setTimeout(function(){ $(window).scrollTop(0); },300);
        $('.carusel').slick({
            slidesToShow: 1,
            dots:false,
            autoplay: true,
            autoplaySpeed: 5000,
            adaptiveHeight: true,
            prevArrow: '<div class="anterior"><i class="icon-angle-left"></i></div>',
            nextArrow: '<div class="siguiente"><i class="icon-angle-right"></i></div>',
        });

        $('.carusel').slickLightbox({
            slick: {
                itemSelector: 'a',
                navigateByKeyboard: true,
                slidesToShow: 1,
                dots:false,
                autoplay: true,
                autoplaySpeed: 5000,
                adaptiveHeight: true
            }
        });

    });
</script>
<#assign isSignedIn = themeDisplay.isSignedIn() >
<#assign url = themeDisplay.getCDNBaseURL()>
<#assign groupId = themeDisplay.getScopeGroupId()>
<#assign Layout = themeDisplay.getLayout()>
<#assign journalArticleId = .vars['reserved-article-id'].data>

<#--${url}<br>${groupId}<br>${Layout}<br>${journalArticleId}-->


<div class="contenedor-noticia-detalle">
    <h2 class="titulo-secundario txt-gris text-center mb-4"><b>${tituloPrincipal.getData()}</b>
        <#if isSignedIn>
            <span class="ica-edicion-detalle">
                <a href="${url}/group/guest/~/control_panel/manage?p_p_id=com_liferay_journal_web_portlet_JournalPortlet&_com_liferay_journal_web_portlet_JournalPortlet_mvcPath=%2Fedit_article.jsp&_com_liferay_journal_web_portlet_JournalPortlet_groupId=${groupId}&_com_liferay_journal_web_portlet_JournalPortlet_articleId=${journalArticleId}" target="_blank">
                    <@liferay_ui["icon"]
                                icon="pencil"
                                markupView="lexicon"
                                message="Editar: " + tituloPrincipal.getData()
                            />
                </a>
            </span>
        </#if>
    </h2>
    <div class="row">
        <div class="col-md-7">
            <div class="fecha-noticia-detalle mb-3">
                <#assign fechaNoticia_Data = getterUtil.getString(fechaNoticia.getData())>
                <#if validator.isNotNull(fechaNoticia_Data)>
                    <#assign fechaNoticia_DateObj = dateUtil.parseDate("yyyy-MM-dd", fechaNoticia_Data, locale)>
                    ${dateUtil.getDate(fechaNoticia_DateObj, "dd/MM/yyyy", locale)}
                </#if>
            </div>
            <div class="descripcion-noticia-detalle">
                ${descripcionNoticia.getData()}
            </div>
        </div>
        <div class="col-md-5 text-center align-self-center">
            <#if caruselImagenes.getSiblings()?has_content>
            <div class="carusel">
                <#list caruselImagenes.getSiblings() as itemImagenes>
                    <#if itemImagenes.getData()?? && itemImagenes.getData() != "">
                        <div><a href="${itemImagenes.getData()}"><img alt="${itemImagenes.getAttribute('alt')}" src="${itemImagenes.getData()}" /></a></div>
                    <#else>
                        <div><img alt="No hay imagen" src="/documents/20124/0/no-img.png/26d39fab-a07b-b4c4-c30b-88e859f6cbe7" /></div>
                    </#if>
                </#list>
                <#if videoInterno?has_content>
                    <#list videoInterno.getSiblings() as itemVideoInt>
                        <#if itemVideoInt.getData()?? && itemVideoInt.getData() != "">
                                <video controls loop>
                                    <source src="${itemVideoInt.getData()}" type="video/mp4">
                                    Su navegador no soporta la etiqueta video.
                                </video>
                            <#else>
                        </#if>
                    </#list>
                </#if>
                <#if videoExterno?has_content>
                    <#list videoExterno.getSiblings() as itemVideoExt>
                        <#if itemVideoExt.getData()?? && itemVideoExt.getData() != "">
                            <div class="video-responsive">
                                <iframe src="https://www.youtube.com/embed/${itemVideoExt.getData()}" frameborder="0" allowfullscreen></iframe>
                            </div>                   
                        <#else>
                        </#if>
                    </#list>
                </#if>
            </div>
            </#if>
        </div>
        <div class="col-md-12">
            <h2 class="mt-4">Enlaces relacionados</h2>
            <div class="contenedor-enlaces-relacionados">
            <#if enlacesRelacionados.getSiblings()?has_content>
                <#list enlacesRelacionados.getSiblings() as itemEnlace>
                    <#if itemEnlace.textoEnlace.getData()?? && itemEnlace.textoEnlace.getData() != "" && itemEnlace.urlEnlace.getData()?? && itemEnlace.urlEnlace.getData() != "">
                        <span></span><a class="" href="${itemEnlace.urlEnlace.getData()}" title="${itemEnlace.textoEnlace.getData()}">${itemEnlace.textoEnlace.getData()}</a><br>
                    <#else>
                        no existen enlaces relacionados
                    </#if>
                </#list>
            </#if>
            </div>
        </div>
    </div>
</div>