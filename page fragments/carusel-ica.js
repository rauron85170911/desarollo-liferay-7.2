const flechasCarusel = configuration.flechasCarusel;
const numeroImagenesCarusel = configuration.numeroImagenesCarusel;
/*console.log(flechasCarusel);
console.log(numeroImagenesCarusel);*/

$(document).ready(function(){
    /* Configuración del slick-carousel */
    $('.carusel-ica').slick({
        arrows: flechasCarusel
    });

    /* Fix edición de liferay en las imagenes (renderiza mal el valor de las imagenes mete como valor etiqueta IMG :S)*/
    const $imagenesRotas = $('lfr-editable img'); // lfr-editable image
    
    $($imagenesRotas).each(function(index){
        var valorImagenRota = $(this).attr('src');
        var inicia = valorImagenRota.indexOf('<img src="');
        if(inicia == 0){
            valorImagenRota = valorImagenRota.replace('<img src="','');
            valorImagenRota = valorImagenRota.replace('">','');
            //console.log(valorImagenRota);
            $(this).attr('src',valorImagenRota)
        }else {
            //console.log('no entro');
        }
    });

});