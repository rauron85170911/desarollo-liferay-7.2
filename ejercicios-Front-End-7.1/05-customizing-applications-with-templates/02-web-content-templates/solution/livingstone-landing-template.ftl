<img src="${Imageiqrh.getData()}">
<div class="card">
    <div class="flex-item-center text-center" style="width: 100%">
        <br/>
        <h1>${Tagline.getData()}</h1>
    </div>
    <div class="card-body">
        <p>${Content.getData()}</p>
    </div>
</div>