<h1 class="text-center">${title.getData()}</h1>

<div class="container">
    <ul class="list-unstyled row">
        <li class="col-md-6">
            <div class="card-type-asset">
                <div class="card-header aspect-ratio">
                    <#if text1.image1.getData()?? && text1.image1.getData() != "">
                        <img alt="${text1.image1.getAttribute("alt")}" class="aspect-ratio-item-fluid" src="${text1.image1.getData()}">
                    </#if>
                </div>
                <div class="card-body">
                    <h2 class="text-center">
                        <a class="item-one" href="${text1.link1.getFriendlyUrl()}">${text1.getData()}</a>
                    </h2>
                </div>
            </div>
        </li>
        <li class="col-md-6">
            <div class="card-type-asset">
                <div class="card-header aspect-ratio">
                    <#if text2.image2.getData()?? && text2.image2.getData() != "">
                        <img alt="${text2.image2.getAttribute("alt")}" class="aspect-ratio-item-fluid" src="${text2.image2.getData()}">
                    </#if>
                </div>
                <div class="card-body">
                    <h2 class="text-center">
                        <a class="item-one" href="${text2.link2.getFriendlyUrl()}">${text2.getData()}</a>
                    </h2>
                </div>
            </div>
        </li>
    </ul>
    <ul class="list-unstyled row">
        <li class="col-md-6">
            <div class="card-type-asset">
                <div class="card-header aspect-ratio">
                    <#if text3.image3.getData()?? && text3.image3.getData() != "">
                        <img alt="${text3.image3.getAttribute("alt")}" class="aspect-ratio-item-fluid" src="${text3.image3.getData()}">
                    </#if>
                </div>
                <div class="card-body">
                    <h2 class="text-center">
                        <a class="item-one" href="${text3.link3.getFriendlyUrl()}">${text3.getData()}</a>
                    </h2>
                </div>
            </div>
        </li>
        <li class="col-md-6">
            <div class="card-type-asset">
                <div class="card-header aspect-ratio">
                    <#if text4.image4.getData()?? && text4.image4.getData() != "">
                        <img alt="${text4.image4.getAttribute("alt")}" class="aspect-ratio-item-fluid" src="${text4.image4.getData()}">
                    </#if>
                </div>
                <div class="card-body">
                    <h2 class="text-center">
                        <a class="item-one" href="${text4.link4.getFriendlyUrl()}">${text4.getData()}</a>
                    </h2>
                </div>
            </div>
        </li>
    </ul>
</div>