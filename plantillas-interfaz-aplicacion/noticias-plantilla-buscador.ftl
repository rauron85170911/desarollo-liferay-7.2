<#list resultadoNoticias as r>
    <#assign tituloPrincipal = r.tituloPrincipal />
    <#assign fechaNoticia = r.fechaNoticia />
    <#assign descripcionNoticia = r.descripcionNoticia />
    <#assign img = r.imagenPrincipal />
    <#assign imgGroupId = img?eval.groupId/>
    <#assign imgName = img?eval.name/>
    <#assign imgUuid = img?eval.uuid/>
    <#assign imgAlt = img?eval.alt/>
    <#assign urlTitle = r.urlTitle />

    <div class="asset-abstract">
        <div class="pull-right">
        </div>
            <div class="lfr-meta-actions asset-actions">
                <#--<@getEditIcon />-->
            </div>
            <div class="row mb-2">
                <div class="col-md-3 text-center align-self-center sei-img">
                <#if imgGroupId?? && imgGroupId != "">
                    <img class="img-responsive img-listados" src="/documents/${imgGroupId}/0/${imgName}/${imgUuid}" alt="${imgAlt}">
                <#else>
                    <img class="img-responsive no-imagen" src="/documents/20124/0/no-img.png/26d39fab-a07b-b4c4-c30b-88e859f6cbe7" alt="No hay imagen">
                </#if>
                </div>
                <div class="col-md-9 align-self-center">
                    <div class="contenedor-noticia">
                        <h2><a href="/web/guest/-/${r.urlTitle}" class="txt-gris">${tituloPrincipal}</a></h2>
                        <div class="fecha-noticia">${fechaNoticia?date("yyyy-MM-dd")?string("dd/MM/yyyy")}</div>
                        <div class="descripcion-noticia" recortar-literal="180">${descripcionNoticia}</div>
                    </div>
                </div>
            </div>
        <div class="asset-content">
            <div class="asset-summary">
            </div>
        </div>
    </div>
</#list>

<#-- Macro de edición -->
<#macro getEditIcon>
    <#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
        <#assign redirectURL = renderResponse.createRenderURL() />

        ${redirectURL.setParameter("struts_action", "/asset_publisher/add_asset_redirect")}
        ${redirectURL.setWindowState("pop_up")}

        <#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />

        <#if validator.isNotNull(editPortletURL)>
            <#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />

            <@liferay_ui["icon"]
                cssClass="icon-monospaced visible-interaction"
                icon="pencil"
                markupView="lexicon"
                message="Editar: " + tituloPrincipal
                url="javascript:Liferay.Util.openWindow({dialog: {width: 960}, id:'" + renderResponse.getNamespace() + "editAsset', title: 'Editar: " + tituloPrincipal + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
            />
        </#if>
    </#if>
</#macro>