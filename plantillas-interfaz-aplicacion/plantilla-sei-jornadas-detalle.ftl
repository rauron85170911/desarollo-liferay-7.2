<#-- Plantilla Jornadas Detalle -->

<script>
    $(document).ready(function(){
        //var $titulo = $('#content > h1').text();
        //console.log($titulo);
        $titulo = $('#content > h1').remove();
        setTimeout(function(){ $(window).scrollTop(0); },300);
        $contenidoAsset = $('.asset-full-content');
        $contenidoAsset.find('h4.component-title').remove();
        $contenidoAsset.find('.metadata-author').remove();
        $contenidoAsset.find('.lfr-asset-anchor').remove();            
    });
</script>

<#assign isSignedIn = themeDisplay.isSignedIn() >
<#assign url = themeDisplay.getCDNBaseURL()>
<#assign groupId = themeDisplay.getScopeGroupId()>
<#assign Layout = themeDisplay.getLayout()>
<#assign journalArticleId = .vars['reserved-article-id'].data>

<#--${url}<br>${groupId}<br>${Layout}<br>${journalArticleId}-->

<div class="contenedor-jornadas-detalle">
    <h2 class="titulo-secundario txt-gris text-center mb-4"><b>${tituloPrincipal.getData()}</b>
        <#if isSignedIn>
            <span class="ica-edicion-detalle">
                <a href="${url}/group/guest/~/control_panel/manage?p_p_id=com_liferay_journal_web_portlet_JournalPortlet&_com_liferay_journal_web_portlet_JournalPortlet_mvcPath=%2Fedit_article.jsp&_com_liferay_journal_web_portlet_JournalPortlet_groupId=${groupId}&_com_liferay_journal_web_portlet_JournalPortlet_articleId=${journalArticleId}" target="_blank">
                    <@liferay_ui["icon"]
                                icon="pencil"
                                markupView="lexicon"
                                message="Editar: " + tituloPrincipal.getData()
                            />
                </a>
            </span>
        </#if>
    </h2>
    <div class="row">
        <div class="col-md-12">
            <div class="fecha-noticia-detalle mb-3">
                <#assign fechaNoticia_Data = getterUtil.getString(fechaNoticia.getData())>
                <#if validator.isNotNull(fechaNoticia_Data)>
                    <#assign fechaNoticia_DateObj = dateUtil.parseDate("yyyy-MM-dd", fechaNoticia_Data, locale)>
                    ${dateUtil.getDate(fechaNoticia_DateObj, "dd/MM/yyyy", locale)}
                </#if>
            </div>
            <div class="descripcion-noticia-detalle">
                ${descripcionNoticia.getData()}
            </div>
        </div>
        <div class="col-md-12">
            <h2 class="mt-4">Enlaces relacionados</h2>
            <div class="contenedor-enlaces-relacionados">
            <#if enlacesRelacionados.getSiblings()?has_content>
                <#list enlacesRelacionados.getSiblings() as itemEnlace>
                    <#if itemEnlace.textoEnlace.getData()?? && itemEnlace.textoEnlace.getData() != "" && itemEnlace.urlEnlace.getData()?? && itemEnlace.urlEnlace.getData() != "">
                        <span></span><a class="" href="${itemEnlace.urlEnlace.getData()}" title="${itemEnlace.textoEnlace.getData()}">${itemEnlace.textoEnlace.getData()}</a><br>
                    <#else>
                        no existen enlaces relacionados
                    </#if>
                </#list>
            </#if>
            </div>
        </div>
    </div>
</div>