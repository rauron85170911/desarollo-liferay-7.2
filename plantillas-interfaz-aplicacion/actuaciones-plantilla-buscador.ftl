<div class="row mb-2">
    <div class="col-md-12">
        <table class="table table-responsive tabla-seiasa">
            <thead>
                <tr>
                    <th>
                        <p align="center"><strong>Nombre</strong></p>
                    </th>
                    <th>
                        <p align="center"><strong>Comunidad Autonoma</strong></p>
                    </th>
                    <th>
                        <p align="center"><strong>Provincia</strong></p>
                    </th>
                    <th>
                        <p align="center"><strong>Situación</strong></p>
                    </th>
                    <th>
                        <p align="center"><strong>Detalle</strong></p>
                    </th>
                </tr>
            </thead>
            <tbody>
                <#list resultadoActuaciones as r>
                <#assign nombre = r.nombre />
                <#--<#assign tituloPrincipal = r.tituloPrincipal />-->
                <#assign comunidadAutonoma = r.comunidadAutonoma />
                <#assign provincia = r.provincia />
                <#assign situacion = r.situacion />
                <#assign entryId= r.entryId?replace('.','')/>
                    <tr>
                        <td>
                            <p align="left">&nbsp;${nombre}</p>
                        </td>
                        <td>
                            <p align="center">${comunidadAutonoma}</p>
                        </td>
                        <td>
                            <p align="center">${provincia}</p>
                        </td>
                        <td>
                            <p align="center">${situacion}</p>
                        </td>
                        <td width="150">
                            <p align="center"><a class="btn btn-outline-primary px-4" href="/web/guest/detalle-actuacion/-/asset_publisher/r19Ajlbdn4Nm?assetEntry=${entryId}">Ver más</a></p>
                        </td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </div>
</div>

<#-- Macro de edición -->
<#macro getEditIcon>
	<#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
		<#assign redirectURL = renderResponse.createRenderURL() />

		${redirectURL.setParameter("struts_action", "/asset_publisher/add_asset_redirect")}
		${redirectURL.setWindowState("pop_up")}

		<#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />

		<#if validator.isNotNull(editPortletURL)>
			<#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />

			<@liferay_ui["icon"]
				cssClass="icon-monospaced visible-interaction"
				icon="pencil"
				markupView="lexicon"
				message="Editar :" + NombreActuacion
				url="javascript:Liferay.Util.openWindow({dialog: {width: 960}, id:'" + renderResponse.getNamespace() + "editAsset', title: 'Editar: " + NombreActuacion + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
			/>
		</#if>
	</#if>
</#macro>