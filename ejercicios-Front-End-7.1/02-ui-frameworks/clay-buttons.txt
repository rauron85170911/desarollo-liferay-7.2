<div class="btn-group">
	<div class="btn-group-item">
		<button class="btn btn-primary" type="button">Book Now</button>
	</div>
	<div class="btn-group-item">
		<button aria-expanded="false" aria-haspopup="true" class="btn btn-secondary btn-group-item dropdown-toggle" data-toggle="dropdown" type="button">See More</button>
		<div class="dropdown-menu">
			<a class="dropdown-item" href="#1">Additional Packages</a>
			<div class="dropdown-divider">&nbsp;</div>
			<a class="dropdown-item" href="#1">More Information</a>
		</div>	
	</div>
</div>
