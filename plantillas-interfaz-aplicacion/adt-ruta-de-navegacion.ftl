<div class="mt-3">
    <span class="elemento-0"><a class="" href="/inicio">Inicio</a>&nbsp;&gt;</span>
    <#if entries?has_content>
        <#assign x = 0>
        <#list entries as curEntry>
            <#assign x++>
            <#if curEntry?is_last>
                <span class="breadcrumb-text-truncate">${curEntry.getTitle()}</span>
            <#else>
                <span class="elemento-${x}">
                    <a href="${curEntry.getURL()}">${curEntry.getTitle()}</a>
                    <#sep>&gt;</#sep>
                </span>
            </#if>
        </#list>
    </#if>
</div>