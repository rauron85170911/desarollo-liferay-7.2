<#-- Liferay versión 7.2 -->

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<!-- configurarlo desde el panel de control -> configuracion ->  configuracion del sistema -> motores de plantilla (eliminar servicelocator de variables restringidas) -->
<#--<#assign AssetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>-->
<#--${AssetEntryLocalService}-->
<#--<#assign LayoutLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.LayoutLocalService")>-->
<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext()>
<#assign themeDisplay = serviceContext.getThemeDisplay() />
<#-- themeDisplay.getLayout() sacar el objeto con todos los parametros -->
<#assign Layout = themeDisplay.getLayout() />
<#assign pagina = Layout.friendlyURL />
<#--${Layout}-->
<#--${pagina}-->

<#list entries as curEntry>

    <#assign assetRenderer = curEntry.getAssetRenderer() />
    <#assign className = assetRenderer.getClassName() />

    <#--<#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, CurEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />-->

    <#if className =="com.liferay.journal.model.JournalArticle">
        <#assign JournalArticle = assetRenderer.getArticle() />
        <#assign document = saxReaderUtil.read(JournalArticle.getContent()) />
        <#assign Titulo = document.valueOf("//dynamic-element[@name='tituloPrincipal']/dynamic-content/text()") />
        <#assign img = document.valueOf("//dynamic-element[@name='imagenPrincipal']/dynamic-content/text()") />
        <#assign imgGroupId = img?eval.groupId/>
        <#assign imgName = img?eval.name/>
        <#assign imgUuid = img?eval.uuid/>
        <#assign imgAlt = img?eval.alt/>
        <#assign Fecha = document.valueOf("//dynamic-element[@name='fechaNoticia']/dynamic-content/text()") />
        <#assign Descripcion = document.valueOf("//dynamic-element[@name='ResumenJornada']/dynamic-content/text()") />
        <#assign EnlaceJornada = document.valueOf("//dynamic-element[@name='enlaceNoticia']/dynamic-content/text()") />
        <#-- URL A DETALLES  -->
        <#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, currentURL) />
        <#--<#assign viewURL2 = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, curEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />-->
        <#--<#assign pageLayout = LayoutLocalService.getLayout(groupId, false, layoutID) />-->
    </#if>

	<div class="asset-abstract">
		<div class="pull-right">
			<#--<@getPrintIcon />-->

			<#--<@getFlagsIcon />-->

			<#--<@getEditIcon />-->
		</div>

		<#--<h3 class="asset-title">
			<a href="${viewURL}">
				${entryTitle}
			</a>
		</h3>-->
        <#--<@getEditIcon />-->
			<div class="lfr-meta-actions asset-actions">
				<@getEditIcon />
			</div>
            <div class="row mb-2">

                <div class="col-md-3 text-center align-self-center sei-img">
                    <img class="img-responsive img-listados" width="260" height="160" src="/documents/${imgGroupId}/0/${imgName}/${imgUuid}" alt="${imgAlt}">
                </div>
                <div class="col-md-9">
                    <div class="contenedor-noticia">
                        <h2><a href="${viewURL}" class="txt-gris">${Titulo}</a></h2>
                        <div class="fecha-noticia">${Fecha?date("yyyy-MM-dd")?string("dd/MM/yyyy")}</div>
						<#if Descripcion?? && Descripcion != "">
							<div class="descripcion-noticia" recortar-literal="180">${Descripcion}</div>
						<#else>
							<div class="descripcion-noticia">No hay Descripción</div>
						</#if>
                        
                    </div>
                </div>
            </div>

		<#--<@getMetadataField fieldName="tags" />-->

		<#--<@getMetadataField fieldName="create-date" />-->

		<#--<@getMetadataField fieldName="view-count" />-->

		<div class="asset-content">
			<#--<@getSocialBookmarks />-->

			<div class="asset-summary">
				<#--<@getMetadataField fieldName="author" />-->

				<!--${htmlUtil.escape(assetRenderer.getSummary(renderRequest, renderResponse))}-->

				<#--<a href="${viewURL}"><@liferay.language key="read-more" /><span class="hide-accessible"><@liferay.language key="about" />${entryTitle}--></span></a>
			</div>

			<#--<@getRatings />-->

			<#--<@getRelatedAssets />-->

			<#--<@getDiscussion />-->
		</div>
	</div>
</#list>

<#-- Macros -->

<#macro getDiscussion>
	<#if getterUtil.getBoolean(enableComments) && assetRenderer.isCommentable()>
		<br />

		<#assign discussionURL = renderResponse.createActionURL() />

		${discussionURL.setParameter("javax.portlet.action", "invokeTaglibDiscussion")}

		<@liferay_comment["discussion"]
			className=curEntry.getClassName()
			classPK=curEntry.getClassPK()
			formAction=discussionURL?string
			formName="fm" + curEntry.getClassPK()
			ratingsEnabled=getterUtil.getBoolean(enableCommentRatings)
			redirect=currentURL
			userId=assetRenderer.getUserId()
		/>
	</#if>
</#macro>

<#macro getEditIcon>
	<#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
		<#assign redirectURL = renderResponse.createRenderURL() />

		${redirectURL.setParameter("struts_action", "/asset_publisher/add_asset_redirect")}
		${redirectURL.setWindowState("pop_up")}

		<#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />

		<#if validator.isNotNull(editPortletURL)>
			<#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />

			<@liferay_ui["icon"]
				cssClass="icon-monospaced visible-interaction"
				icon="pencil"
				markupView="lexicon"
				message="Editar: " + Titulo
				url="javascript:Liferay.Util.openWindow({dialog: {width: 960}, id:'" + renderResponse.getNamespace() + "editAsset', title: 'Editar: " + Titulo + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
			/>
		</#if>
	</#if>
</#macro>

<#macro getFlagsIcon>
	<#if getterUtil.getBoolean(enableFlags)>
		<@liferay_flags["flags"]
			className=curEntry.getClassName()
			classPK=curEntry.getClassPK()
			contentTitle=curEntry.getTitle(locale)
			label=false
			reportedUserId=curEntry.getUserId()
		/>
	</#if>
</#macro>

<#macro getMetadataField
	fieldName
>
	<#if stringUtil.split(metadataFields)?seq_contains(fieldName)>
		<span class="metadata-entry metadata-${fieldName}">
			<#assign dateFormat = "dd MMM yyyy - HH:mm:ss" />

			<#if stringUtil.equals(fieldName, "author")>
				<@liferay.language key="by" /> ${htmlUtil.escape(portalUtil.getUserName(assetRenderer.getUserId(), assetRenderer.getUserName()))}
			<#elseif stringUtil.equals(fieldName, "categories")>
				<@liferay_asset["asset-categories-summary"]
					className=curEntry.getClassName()
					classPK=curEntry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif stringUtil.equals(fieldName, "create-date")>
				${dateUtil.getDate(curEntry.getCreateDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "expiration-date")>
				${dateUtil.getDate(curEntry.getExpirationDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "modified-date")>
				${dateUtil.getDate(curEntry.getModifiedDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "priority")>
				${curEntry.getPriority()}
			<#elseif stringUtil.equals(fieldName, "publish-date")>
				${dateUtil.getDate(curEntry.getPublishDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "tags")>
				<@liferay_asset["asset-tags-summary"]
					className=curEntry.getClassName()
					classPK=curEntry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif stringUtil.equals(fieldName, "view-count")>
				${curEntry.getViewCount()} <@liferay.language key="views" />
			</#if>
		</span>
	</#if>
</#macro>

<#macro getPrintIcon>
	<#if getterUtil.getBoolean(enablePrint)>
		<#assign printURL = renderResponse.createRenderURL() />

		${printURL.setParameter("mvcPath", "/view_content.jsp")}
		${printURL.setParameter("assetEntryId", curEntry.getEntryId()?string)}
		${printURL.setParameter("viewMode", "print")}
		${printURL.setParameter("type", curEntry.getAssetRendererFactory().getType())}
		${printURL.setWindowState("pop_up")}

		<@liferay_ui["icon"]
			icon="print"
			markupView="lexicon"
			message="print"
			url="javascript:Liferay.Util.openWindow({id:'" + renderResponse.getNamespace() + "printAsset', title: '" + languageUtil.format(locale, "print-x-x", ["hide-accessible", entryTitle], false) + "', uri: '" + htmlUtil.escapeURL(printURL.toString()) + "'});"
		/>
	</#if>
</#macro>

<#macro getRatings>
	<#if getterUtil.getBoolean(enableRatings) && assetRenderer.isRatable()>
		<div class="asset-ratings">
			<@liferay_ui["ratings"]
				className=curEntry.getClassName()
				classPK=curEntry.getClassPK()
			/>
		</div>
	</#if>
</#macro>

<#macro getRelatedAssets>
	<#if getterUtil.getBoolean(enableRelatedAssets)>
		<@liferay_asset["asset-links"]
			assetEntryId=curEntry.getEntryId()
			viewInContext=!stringUtil.equals(assetLinkBehavior, "showFullContent")
		/>
	</#if>
</#macro>

<#macro getSocialBookmarks>
	<@liferay_social_bookmarks["bookmarks"]
		className=curEntry.getClassName()
		classPK=curEntry.getClassPK()
		displayStyle="${socialBookmarksDisplayStyle}"
		target="_blank"
		title=curEntry.getTitle(locale)
		url=viewURL
	/>
</#macro>