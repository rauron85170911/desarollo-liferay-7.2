<#--
Widget templates can be used to modify the look of a
specific application.

Please use the left panel to quickly add commonly used variables.
Autocomplete is also available and can be invoked by typing "${".
-->


<#-- Plantilla Actuaciones Detalle -->

<script>
    $(document).ready(function(){
        //var $titulo = $('#content > h1').text();
        //console.log($titulo);
        $titulo = $('#content > h1').remove();
        $contenidoAsset = $('.asset-full-content');
        $contenidoAsset.find('h4.component-title').remove();
        $contenidoAsset.find('.metadata-author').remove();
        $contenidoAsset.find('.lfr-asset-anchor').remove();

        if(screen.width < 768) {
            $('.carusel').slick({
                slidesToShow: 1,
                dots:false,
                autoplay: true,
                autoplaySpeed: 5000,
                prevArrow: '<div class="anterior"><i class="icon-angle-left"></i></div>',
                nextArrow: '<div class="siguiente"><i class="icon-angle-right"></i></div>',
            });
            $('.carusel').slickLightbox({
                slick: {
                    itemSelector: 'a',
                    navigateByKeyboard: true,
                    slidesToShow: 1,
                    dots:false,
                    autoplay: true,
                    autoplaySpeed: 5000,
                }
            });
        } else {
            $('.carusel').slick({
                slidesToShow: 3,
                dots:false,
                autoplay: true,
                autoplaySpeed: 5000,
                prevArrow: '<div class="anterior"><i class="icon-angle-left"></i></div>',
                nextArrow: '<div class="siguiente"><i class="icon-angle-right"></i></div>',
            });
            $('.carusel').slickLightbox({
                slick: {
                slidesToShow: 1,
                dots:false,
                autoplay: true,
                autoplaySpeed: 5000,
                }
            });
        }
    });
</script>

<#assign LayoutLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.LayoutLocalService")>
<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#--<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.portlet.asset.service.AssetLinkLocalService") />-->
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />

<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext()>
<#assign themeDisplay = serviceContext.getThemeDisplay() />
<#-- themeDisplay.getLayout() sacar el objeto con todos los parametros -->
<#assign Layout = themeDisplay.getLayout() />
<#assign pagina = Layout.getResetMaxStateURL(request) />


<#--<#assign idEntry = request.getParameter('assetEntry')/>-->

<#-- <p>CurrentURL: ${themeDisplay.getURLCurrent()}</p> -->

<#assign currentURL = themeDisplay.getURLCurrent()/>
<#assign ultimo = currentURL?last_index_of("/") />
<#assign idEntry = currentURL?substring(ultimo + 1) />
<#--<p>EntryID: ${idEntry}</p>-->

<#if entries?has_content>
    
	<#list entries as curEntry>
	    <#assign entry = curEntry/>
	    <#--ENTRY: ${entry}--> 
	    <#if entry.getEntryId()?string?trim = idEntry?string?trim>
       
       <#--
           <#assign assetEntry = assetEntryLocalService.getAssetEntry(entry.getEntryId())/>
           <#assign resourcePrimKey = assetEntry.getClassPK()/>
           <#assign currentArticle = journalArticleLocalService.getArticle(groupId,  ${.Vars['reserved-article-title'].Data}) />
         -->  
            <#assign currentArticleAssetEntryId =  entry.getEntryId()/>
            <#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />
            <#--
            <#if (currentArticleRelatedLinks?? && currentArticleRelatedLinks?size != 0)>
                <#list currentArticleRelatedLinks as related_entry>
                    <#assign relatedAssetEntryId = related_entry.getEntryId2() />
                    <#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
                    <#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
                    <#assign relatedArticle = journalArticleLocalService.getLatestArticle(relatedAssetEntryPrimKey) />
                    <#assign relatedArticleId = relatedArticle.getArticleId() />
                    Related Article ID: ${relatedArticleId}<br>
                </#list>
            <#else>
               <b> No HAY ARTICULOS RELACIONADOS<b>
            </#if>
            -->

            <#--ENTRY: ${curEntry} -->
            <#assign assetRenderer = curEntry.getAssetRenderer() />
            <#assign className = assetRenderer.getClassName() />
            <#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry,true) >
    		<#assign document = saxReaderUtil.read(curEntry.getAssetRenderer().getArticle().getContent()) >
            <#-- DATOS DE LA ACTUACION -->
             <#if className =="com.liferay.journal.model.JournalArticle">
                <#assign JournalArticle = assetRenderer.getArticle() />
                <#assign document = saxReaderUtil.read(JournalArticle.getContent()) />
                <#assign nombre=document.valueOf("//dynamic-element[@name='nombre']/dynamic-content/text()") />	
                <#assign objetivo=document.valueOf("//dynamic-element[@name='objetivo']/dynamic-content/text()") />	
                <#assign descripcion=document.valueOf("//dynamic-element[@name='descripcion']/dynamic-content/text()") />
                <#assign ubicacion=document.valueOf("//dynamic-element[@name='ubicacion']/dynamic-content/text()") />
                <#assign situacion=document.valueOf("//dynamic-element[@name='situacion']/dynamic-content/text()") />
                <#assign imagenesActuacion = document.getRootElement().selectNodes("dynamic-element[@name='imagenesActuacion']") />
            </#if>

            <div class="contenedor-actuacion-detalle">
                <h1 class="text-center mb-4">${nombre}</h1>
                <div class="row">
                    <div class="col-md-12 text-center align-self-center">
                       <#if imagenesActuacion?has_content>
                            <div class="carusel">
                                <#list imagenesActuacion as itemImagenes>
                                    <#if itemImagenes?? && itemImagenes != "">
                                        <#assign content = itemImagenes.valueOf("dynamic-content/text()")/>
                                        <#assign imgGroupId =  content?eval.groupId/>
                                        <#assign imgName = content?eval.name/>
                                        <#assign imgUuid = content?eval.uuid/>
                                        <#assign imgAlt = content?eval.alt/>
                                        <div><a href="/documents/${imgGroupId}/0/${imgName}/${imgUuid}" alt="${imgAlt}"><img width="" height="" src="/documents/${imgGroupId}/0/${imgName}/${imgUuid}" alt="${imgAlt}" /></a></div>
                                    <#else>
                                        <div><img width="" height="" alt="No hay imagen" src="/documents/20124/0/no-img.png/26d39fab-a07b-b4c4-c30b-88e859f6cbe7" /></div>
                                     
                                    </#if>
                                   
                                </#list>                           
                            </div>
                        <#else>
                            <div class="carusel">
                                <div><img width="" height="" alt="No hay imagen" src="/documents/20124/0/no-img.png/26d39fab-a07b-b4c4-c30b-88e859f6cbe7" /></div>
                            </div>
                        </#if>
                      
                    </div>
                    <div class="col-md-7 mt-4 contenedor-datos">
                        <h2 class="txt-gris mt-4">Objetivo</h2>
                        <#if objetivo?? && objetivo != "">
                            <p class="mt-3">${objetivo}</p>
                        <#else>
                            <p class="mt-3">No hay Objetivo</p>
                        </#if>
                        <h2 class="txt-gris">Descripción</h2>
                        <#if descripcion?? && descripcion != "">
                            <p class="mt-3">${descripcion}</p>
                        <#else>
                            <p class="mt-3">No hay Descripción</p>
                        </#if>
                        <h2 class="txt-gris">Ubicación</h2>
                        <#if ubicacion?? && ubicacion != "">
                            <p class="mt-3">${ubicacion}</p>
                        <#else>
                            <p class="mt-3">No hay Ubicación</p>
                        </#if>
                        <h2 class="txt-gris">Estado</h2>
                        <#if situacion?? && situacion != "">
                            <p class="mt-3">${situacion}</p>
                        <#else>
                            <p class="mt-3">No hay Estado</p>
                        </#if>
                    </div>
                    <div class="col-md-5 contenedor-cabcolor mt-4">
                        <div class="cabecera-cabcolor fd-gris-medio-oscuro contenedor-actuaciones-noticias">
                            <h2 class="sin-borde">Noticias de esta Actuación</h2>
                        </div>
                        <div class="borde-gris">
                            <#if (currentArticleRelatedLinks?? && currentArticleRelatedLinks?size != 0)>
                                <#list currentArticleRelatedLinks as related_entry>
                                    <#assign relatedAssetEntryId = related_entry.getEntryId2() />
                                    <#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
                                    <#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
                                    <#assign relatedArticle = journalArticleLocalService.getLatestArticle(relatedAssetEntryPrimKey) />
                                    <#assign relatedArticleId = relatedArticle.getArticleId() />
                                    <#-- Related Article ID: ${relatedArticleId}<br> -->
                                    <#assign document = saxReaderUtil.read(relatedArticle.getContent()) />
                                    <#assign titulo=document.valueOf("//dynamic-element[@name='tituloPrincipal']/dynamic-content/text()") />
                                   <#-- Titulo: ${titulo}	 -->
                                    <#assign fecha=document.valueOf("//dynamic-element[@name='fechaNoticia']/dynamic-content/text()") />
                                   <#-- Fecha: ${fecha} -->
                                    <#assign descripcion=document.valueOf("//dynamic-element[@name='descripcionNoticia']/dynamic-content/text()") />	
                                    <#-- Descripcion: ${descripcion} -->
                                    <!-- CONTENIDO DE LA NOTICIA -->
                                    <#--<div class="fecha-actuacion-noticia mt-2">${dateUtil.getDate(fecha, "dd/MM/yyyy", locale)}</div>-->
                                    <#--<div class="fecha-actuacion-noticia mt-2">${fecha?string?datetime?string('dd-MM-yyyy')}</div>-->
                                    <#-- ${mydate?date(“MM/dd/yyyy”)?string(“dd/MM/yyyy”) -->
                                    <div class="fecha-actuacion-noticia mt-2">${fecha?date("yyyy-MM-dd")?string("dd/MM/yyyy")}</div>
                                    <div class="contenedor-actuacion-noticia">
                                        <p>${descripcion}</p>
                                    </div>
                                </#list>
                            <#else>
                                <p class="mt-3">No hay noticias para esta actuación</p>
                            </#if>
                        </div>                            
                    </div>
                </div>
            </div>
        </#if>
    </#list>
</#if>