<#-- Insert 13-simple-template here -->
<img src="${Imageiqrh.getData()}">
<div class="card">
    <div class="flex-item-center text-center" style="width:100%;">
        <br>
        <h1>${TagLine.getData()}</h1>
    </div>
    <div class="card-body">
        <p>${Content.getData()}</p>
    </div>
</div>