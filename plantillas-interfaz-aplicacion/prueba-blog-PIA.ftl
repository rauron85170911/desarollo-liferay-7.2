<#assign tagsService = utilLocator.findUtil("com.liferay.asset.kernel.service.AssetTagLocalService") />
<#if entries?has_content>
    <#list entries as curBlogEntry>

        <#assign viewURL = renderResponse.createRenderURL()/>      

        ${viewURL.setParameter("mvcRenderCommandName", "/blogs/view_entry")}
        ${viewURL.setParameter("redirect", currentURL)}
        ${viewURL.setParameter("urlTitle", curBlogEntry.getUrlTitle())}
        ${viewURL.setParameter("p_p_state", "maximized")}

        <#assign editURL = renderResponse.createRenderURL()/>      
        
        ${editURL.setParameter("mvcRenderCommandName", "/blogs/edit_entry")}
        ${editURL.setParameter("redirect", currentURL)}
        ${editURL.setWindowState(windowStateFactory.getWindowState("MAXIMIZED"))}
        ${editURL.setParameter("entryId", curBlogEntry.getEntryId()?string)}
                   
        <#--<#assign blogContent = curBlogEntry.getContent()/>
        <#list blogContent?html?split("src=&quot;") as x>
            <#if x?starts_with("/documents")>
                <#list x?split("&quot;") as y>
                    <#if y?starts_with("/documents")>
                        <#assign src = y/>                        
                    </#if>
                </#list>
            </#if>
        </#list>-->
        
       <div class="entry approved blog-list">
            <div class="entry-content">
                <#if curBlogEntry.getEntryImageURL(themeDisplay)??>
                <#assign blogContent = curBlogEntry.getContent()/>
                <#list blogContent?html?split("src=&quot;") as x>
                    <#if x?index = 1>
                        <#if x?starts_with("/documents")>
                            <#list x?split("&quot;") as y>
                                <#if y?starts_with("/documents")>
                                    <#assign src = y/>
                                    <div class="entry-image" style="background-image: url(${src});"></div>
                                </#if>
                            </#list>
                        </#if>
                    </#if>
                </#list>
                <#-- Esta es la imagen con reescalado de liferay -->
                <div class="entry-image" style="background-image: url(${curBlogEntry.getSmallImageURL(ThemeDisplay)});"></div>
                </#if>
                
                <div class="entry-date"> <span class="hide-accessible">Fecha de publicación</span>${curBlogEntry.displayDate?string("dd MMM yyyy")}</div>
            </div>
                <div class="entry-title">
                    <h2>${curBlogEntry.getTitle()}</h2>
                </div>
                
                    <#if blogsEntryPermission.contains(permissionChecker, curBlogEntry, 'UPDATE')>
                    <ul class="edit-actions entry icons-container lfr-meta-actions">
                        <li class="edit-entry"> <span class=""> <a href="${editURL}"
                                    class="taglib-icon"> <img src="https://www.grupoica.com/ICA-WEB-theme/images/spacer.png"
                                        alt="" >
                                    <span class="taglib-text ">Editar</span> </a> </span> </li>
                    </ul>
                </#if>
                
            
            
            <ul class="edit-tags">
                <#list tagsService.getTags("com.liferay.portlet.blogs.model.BlogsEntry", curBlogEntry.entryId) as tag>
                  <#assign tagURL = renderResponse.createRenderURL()/>      
                      ${tagURL.setParameter("tag", tag.name )}               
                    <li><a href="${tagURL}">${tag.name}</a></li>
                </#list>
            </ul>
            <div class="entry-body"> <a href="${viewURL}">Leer
                    más <span class="hide-accessible">sobre ${curBlogEntry.getTitle()}</span> »</a> 
            </div>
        </div>
           
    </#list>
 </#if>