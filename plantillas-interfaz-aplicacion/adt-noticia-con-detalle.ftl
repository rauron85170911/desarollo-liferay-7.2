<#-- Liferay versión 7.1 -->

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />

<#list entries as curEntry>

    <#assign assetRenderer = curEntry.getAssetRenderer() />
    <#assign className = assetRenderer.getClassName() />

    <#--<#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, CurEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />-->

    <#if className =="com.liferay.journal.model.JournalArticle">
        <#assign JournalArticle = assetRenderer.getArticle() />
        <#assign document = saxReaderUtil.read(JournalArticle.getContent()) />
        <#assign Titulo = document.valueOf("//dynamic-element[@name='Titulo']/dynamic-content/text()") />
        <#assign Descripcion = document.valueOf("//dynamic-element[@name='Descripcion']/dynamic-content/text()") />
        <#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, currentURL) />
        <#assign viewURL2 = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, curEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />
    </#if>

	<div class="asset-abstract">
		<div class="pull-right">
			<#--<@getPrintIcon />-->

			<#--<@getFlagsIcon />-->

			<#--<@getEditIcon />-->
		</div>

		<#--<h3 class="asset-title">
			<a href="${viewURL}">
				${entryTitle}
			</a>
		</h3>-->
        <#--<@getEditIcon />-->
        <h3>Campos plantilla principal</h3>
        <div class="prueba">
            Titulo: ${Titulo}<br>
            <#--Descripcion: ${Descripcion}<br>-->
            url: ${viewURL}<br>
            url2: ${viewURL2}<br>
            <a href="${viewURL}">url 1</a>
            <a href="${viewURL2}">url 2</a>
        </div>
		<#--<@getMetadataField fieldName="tags" />-->

		<#--<@getMetadataField fieldName="create-date" />-->

		<#--<@getMetadataField fieldName="view-count" />-->

		<div class="asset-content">
			<#--<@getSocialBookmarks />-->

			<div class="asset-summary">
				<#--<@getMetadataField fieldName="author" />-->

				<!--${htmlUtil.escape(assetRenderer.getSummary(renderRequest, renderResponse))}-->

				<#--<a href="${viewURL}"><@liferay.language key="read-more" /><span class="hide-accessible"><@liferay.language key="about" />${entryTitle}--></span></a>
			</div>

			<#--<@getRatings />-->

			<#--<@getRelatedAssets />-->

			<#--<@getDiscussion />-->
		</div>
	</div>
</#list>

<#-- Macros -->

<#macro getDiscussion>
	<#if getterUtil.getBoolean(enableComments) && assetRenderer.isCommentable()>
		<br />

		<#assign discussionURL = renderResponse.createActionURL() />

		${discussionURL.setParameter("javax.portlet.action", "invokeTaglibDiscussion")}

		<@liferay_comment["discussion"]
			className=curEntry.getClassName()
			classPK=curEntry.getClassPK()
			formAction=discussionURL?string
			formName="fm" + curEntry.getClassPK()
			ratingsEnabled=getterUtil.getBoolean(enableCommentRatings)
			redirect=currentURL
			userId=assetRenderer.getUserId()
		/>
	</#if>
</#macro>

<#macro getEditIcon>
	<#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
		<#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("NORMAL"), themeDisplay.getURLCurrent())!"" />

		<#if validator.isNotNull(editPortletURL)>
			<#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />

			<@liferay_ui["icon"]
				cssClass="icon-monospaced visible-interaction"
				icon="pencil"
				markupView="lexicon"
				message=title
				url=editPortletURL.toString()
			/>
		</#if>
	</#if>
</#macro>

<#macro getFlagsIcon>
	<#if getterUtil.getBoolean(enableFlags)>
		<@liferay_flags["flags"]
			className=curEntry.getClassName()
			classPK=curEntry.getClassPK()
			contentTitle=curEntry.getTitle(locale)
			label=false
			reportedUserId=curEntry.getUserId()
		/>
	</#if>
</#macro>

<#macro getMetadataField
	fieldName
>
	<#if stringUtil.split(metadataFields)?seq_contains(fieldName)>
		<span class="metadata-entry metadata-${fieldName}">
			<#assign dateFormat = "dd MMM yyyy - HH:mm:ss" />

			<#if stringUtil.equals(fieldName, "author")>
				<@liferay.language key="by" /> ${htmlUtil.escape(portalUtil.getUserName(assetRenderer.getUserId(), assetRenderer.getUserName()))}
			<#elseif stringUtil.equals(fieldName, "categories")>
				<@liferay_asset["asset-categories-summary"]
					className=curEntry.getClassName()
					classPK=curEntry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif stringUtil.equals(fieldName, "create-date")>
				${dateUtil.getDate(curEntry.getCreateDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "expiration-date")>
				${dateUtil.getDate(curEntry.getExpirationDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "modified-date")>
				${dateUtil.getDate(curEntry.getModifiedDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "priority")>
				${curEntry.getPriority()}
			<#elseif stringUtil.equals(fieldName, "publish-date")>
				${dateUtil.getDate(curEntry.getPublishDate(), dateFormat, locale)}
			<#elseif stringUtil.equals(fieldName, "tags")>
				<@liferay_asset["asset-tags-summary"]
					className=curEntry.getClassName()
					classPK=curEntry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif stringUtil.equals(fieldName, "view-count")>
				${curEntry.getViewCount()} <@liferay.language key="views" />
			</#if>
		</span>
	</#if>
</#macro>

<#macro getPrintIcon>
	<#if getterUtil.getBoolean(enablePrint)>
		<#assign printURL = renderResponse.createRenderURL() />

		${printURL.setParameter("mvcPath", "/view_content.jsp")}
		${printURL.setParameter("assetEntryId", curEntry.getEntryId()?string)}
		${printURL.setParameter("viewMode", "print")}
		${printURL.setParameter("type", curEntry.getAssetRendererFactory().getType())}
		${printURL.setWindowState("pop_up")}

		<@liferay_ui["icon"]
			icon="print"
			markupView="lexicon"
			message="print"
			url="javascript:Liferay.Util.openWindow({id:'" + renderResponse.getNamespace() + "printAsset', title: '" + languageUtil.format(locale, "print-x-x", ["hide-accessible", entryTitle], false) + "', uri: '" + htmlUtil.escapeURL(printURL.toString()) + "'});"
		/>
	</#if>
</#macro>

<#macro getRatings>
	<#if getterUtil.getBoolean(enableRatings) && assetRenderer.isRatable()>
		<div class="asset-ratings">
			<@liferay_ui["ratings"]
				className=curEntry.getClassName()
				classPK=curEntry.getClassPK()
			/>
		</div>
	</#if>
</#macro>

<#macro getRelatedAssets>
	<#if getterUtil.getBoolean(enableRelatedAssets)>
		<@liferay_asset["asset-links"]
			assetEntryId=curEntry.getEntryId()
			viewInContext=!stringUtil.equals(assetLinkBehavior, "showFullContent")
		/>
	</#if>
</#macro>

<#macro getSocialBookmarks>
	<@liferay_social_bookmarks["bookmarks"]
		className=curEntry.getClassName()
		classPK=curEntry.getClassPK()
		displayStyle="${socialBookmarksDisplayStyle}"
		target="_blank"
		title=curEntry.getTitle(locale)
		url=viewURL
	/>
</#macro>