<!-- la sintasis de freemarker cambia -->
[#assign x = configuration.numeroImagenesCarusel]
[#assign theme_display = themeDisplay]

<div class="carusel-ica">
    [#list 1..x as x]
        <div><lfr-editable id="imagen-${x}" type="image"><img src="${theme_display.getPathThemeImages()}/1024x768.png"></lfr-editable></div>
    [/#list]
</div>
<!--<div><lfr-editable id="imagen-1" type="image"><img src="http://via.placeholder.com/1200x300"></lfr-editable></div>
<div><lfr-editable id="imagen-2" type="image"><img src="http://via.placeholder.com/1200x300"></lfr-editable></div>
<div><lfr-editable id="imagen-3" type="image"><img src="http://via.placeholder.com/1200x300"></lfr-editable></div>-->